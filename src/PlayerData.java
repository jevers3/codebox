import java.io.Serializable;


public class PlayerData implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 209128636579951840L;
	private String name;
	private String lastName;
	private String birthDate;
	
	private int score;
	
	PlayerData(String name, String lastName, String birthDate)
	{
		this.name = name;
		this.lastName = lastName;
		this.birthDate = birthDate;
		
		this.score = 0;
	}
	
	public void addScore(int score)
	{
		this.score += score;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public String getBirthDate()
	{
		return birthDate;
	}

	public void setBirthDate(String birthDate)
	{
		this.birthDate = birthDate;
	}

	public int getScore()
	{
		return score;
	}

	public void setScore(int score)
	{
		this.score = score;
	}
	
	public void resetScore()
	{
		this.score = 0;
	}
	
	@Override
	public String toString()
	{
		String result = "";
		
		result += "name: " + name;
		result += " lastname: " + lastName;
		result += " birthdate: " + birthDate;
		result += " score: " + score;
		
		return result;
	}

}
