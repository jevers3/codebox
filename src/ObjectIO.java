import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;


public class ObjectIO
{
	ArrayList<PlayerData> playerData = new ArrayList<PlayerData>();
	
	ObjectIO()
	{
		addPlayerData();
		
		try
		{
			storePlayerData();
		} catch (FileNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		try
		{
			System.out.println("reading .dat file");
			readPlayerData();
		} catch (FileNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void addPlayerData()
	{
		playerData.add(new PlayerData("henk1", "henken", "1991") );
		playerData.add(new PlayerData("henk2", "henken", "1991") );
		playerData.add(new PlayerData("henk3", "henken", "1991") );
		playerData.add(new PlayerData("henk4", "henken", "1991") );
		playerData.add(new PlayerData("henk5", "henken", "1991") );
		playerData.add(new PlayerData("henk6", "henken", "1991") );
		playerData.add(new PlayerData("henk7", "henken", "1991") );
		playerData.add(new PlayerData("henk8", "henken", "1991") );
		playerData.add(new PlayerData("henk9", "henken", "9999") );
	}
	
	private void storePlayerData() throws FileNotFoundException, IOException
	{
		ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream("PlayerData.dat", false));
		
		output.writeObject(playerData);
		
		output.close();
	}
	
	private void readPlayerData() throws FileNotFoundException, IOException
	{
		ObjectInputStream input = new ObjectInputStream(new FileInputStream("PlayerData.dat"));
		
		try
		{
			ArrayList<PlayerData> readPlayerData = (ArrayList<PlayerData>)input.readObject();
			System.out.println(readPlayerData);
		} catch (ClassNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
